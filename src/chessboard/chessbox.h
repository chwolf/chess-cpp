#ifndef CHESSBOX_H
#define CHESSBOX_H

#include <QGraphicsRectItem>
#include <QBrush>
#include "src/piece/chesspiece.h"
#include <QGraphicsSceneMouseEvent>
#include <src/piece/pawn.h>

#include "src/piece/rook.h"

class ChessPiece;
class ChessBox: public QGraphicsRectItem
{
public:
    ChessBox(QGraphicsItem *parent=nullptr);
    ~ChessBox();

    //public member function
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void setColor(QColor color);
    void placePiece(ChessPiece *piece);
    void placePiece(Rook *piece);

    void setOriginalColor(QColor value);
    void resetOriginalColor();

    void setHasChessPiece(bool value,ChessPiece *piece = nullptr);
    bool getHasChessPiece();

    void checkForCheck();

    void setChessPieceColor(QString value);
    QString getChessPieceColor() ;


    int xLoc;
    int yLoc;

    ChessPiece *rook = nullptr;

    ChessPiece * currentPiece;

private:
    QBrush brush;
    QColor originalColor;
    bool hasChessPiece;
    QString chessPieceColor;
};

#endif // CHESSBOX_H
