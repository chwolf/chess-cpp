#include <src/piece/king.h>
#include "chessbox.h"
#include "src/gamemanager.h"
#include <QDebug>


extern GameManager *game;

ChessBox::ChessBox(QGraphicsItem *parent) : QGraphicsRectItem(parent) {
    setRect(0, 0, 100, 100);
    brush.setStyle(Qt::SolidPattern);
    setZValue(-1);
    setHasChessPiece(false);
    setChessPieceColor("NONE");
    currentPiece = nullptr;
}

ChessBox::~ChessBox() {
    delete this;

}

void ChessBox::mousePressEvent(QGraphicsSceneMouseEvent *event) {
    if (currentPiece == game->pieceToMove && currentPiece) {
        currentPiece->mousePressEvent(event);
        return;
    }

    if (game->pieceToMove) {
        if (this->getChessPieceColor() == game->pieceToMove->getSide()) {
            return;
        }
        QList<ChessBox *> movLoc = game->pieceToMove->getLocation();
        int check = 0;
        for (int i = 0, n = movLoc.size(); i < n; i++) {
            if (movLoc[i] == this) {
                check++;
            }
        }
        if (check == 0) {
            return;
        }
        game->pieceToMove->decolor();


        if (this->getHasChessPiece()) {
            this->currentPiece->setIsPlaced(false);
            this->currentPiece->setCurrentBox(nullptr);
            game->placeInDeadPlace(this->currentPiece);
        }

        if (!game->endGame) {
            game->pieceToMove->getCurrentBox()->setHasChessPiece(false);
            game->pieceToMove->getCurrentBox()->currentPiece = nullptr;
            game->pieceToMove->getCurrentBox()->resetOriginalColor();

            if (game->rookToMove != nullptr) {
                game->rookToMove->getCurrentBox()->setHasChessPiece(false);
                game->rookToMove->getCurrentBox()->currentPiece = nullptr;
                game->rookToMove->getCurrentBox()->resetOriginalColor();
                placePiece(game->rookToMove);
                game->rookToMove = nullptr;
            }

            placePiece(game->pieceToMove);


            if (game->pawnToMove != nullptr) {
                if (game->pieceToMove->getCurrentBox()->xLoc == game->pawnToMove->getCurrentBox()->xLoc) {
                    game->pawnToMove->getCurrentBox()->setHasChessPiece(false);
                    game->pawnToMove->getCurrentBox()->currentPiece = nullptr;
                    game->pawnToMove->getCurrentBox()->resetOriginalColor();
                    game->placeInDeadPlace(game->pawnToMove);
                    game->pawnToMove = nullptr;
                }
            }
            if (game->pieceToMove->firstMove && game->pieceToMove->getPieceName() == "pawn") {
                int x = game->pieceToMove->getCurrentBox()->xLoc;
                int y = game->pieceToMove->getCurrentBox()->yLoc;
                if (game->collection[x + 1][y]->getHasChessPiece()) {
                    if (x < 7 && game->collection[x + 1][y]->currentPiece->getPieceName() == "pawn" &&
                        game->collection[x + 1][y]->currentPiece->getSide() != game->pieceToMove->getSide()) {
                        game->pawnToMove = dynamic_cast<Pawn *>(game->pieceToMove);
                        game->pawnToMove->direction = "LEFT";
                    }

                }
                if (game->collection[x - 1][y]->getHasChessPiece()) {
                    if (x > 0 && game->collection[x - 1][y]->currentPiece->getPieceName() == "pawn" &&
                        game->collection[x - 1][y]->currentPiece->getSide() != game->pieceToMove->getSide()) {
                        game->pawnToMove = dynamic_cast<Pawn *>(game->pieceToMove);
                        game->pawnToMove->direction = "RIGHT";

                    }
                }

            }

            game->pieceToMove->firstMove = false;

//            qDebug() << "piecetomove box" << game->pieceToMove->getCurrentBox()->rook;


            game->pieceToMove = nullptr;
            game->changeTurn();
            checkForCheck();
        } else {
            game->endGame = false;
            game->pieceToMove->getCurrentBox()->currentPiece = nullptr;
            game->pieceToMove->getCurrentBox()->resetOriginalColor();
            game->pieceToMove = nullptr;
        }


    } else if (this->getHasChessPiece()) {
        this->currentPiece->mousePressEvent(event);

    }
}

void ChessBox::setHasChessPiece(bool value, ChessPiece *piece) {
    hasChessPiece = value;
    if (value) {
        setChessPieceColor(piece->getSide());
    } else {
        setChessPieceColor("NONE");
    }
}

bool ChessBox::getHasChessPiece() {
    return hasChessPiece;
}

void ChessBox::setOriginalColor(QColor value) {
    originalColor = value;
    setColor(originalColor);
}

void ChessBox::setColor(QColor color) {
    brush.setColor(color);
    setBrush(color);
}

void ChessBox::setChessPieceColor(QString value) {
    chessPieceColor = value;
}

QString ChessBox::getChessPieceColor() {
    return chessPieceColor;
}

void ChessBox::placePiece(ChessPiece *piece) {
    piece->setPos(x() + 50 - piece->pixmap().width() / 2, y() + 50 - piece->pixmap().width() / 2);
    piece->setCurrentBox(this);
    setHasChessPiece(true, piece);
    currentPiece = piece;
}

void ChessBox::placePiece(Rook *piece) {
    int xPiece = piece->getCurrentBox()->xLoc;
    int yPiece = piece->getCurrentBox()->yLoc;
    qDebug() << "y: " << yPiece << "x: " << xPiece;


    if (piece->direction == "RIGHT") {
        piece->setPos(x() - 50 - piece->pixmap().width() / 2, y() + 50 - piece->pixmap().width() / 2);
        piece->setCurrentBox(game->collection[xPiece - 2][yPiece]);
        game->collection[xPiece - 2][yPiece]->setHasChessPiece(true, piece);
//    game->collection[5][7]->setHasChessPiece(true, piece);
        currentPiece = piece;
    } else {
        piece->setPos(x() + 150 - piece->pixmap().width() / 2, y() + 50 - piece->pixmap().width() / 2);
        piece->setCurrentBox(game->collection[xPiece + 3][yPiece]);
        game->collection[xPiece + 3][yPiece]->setHasChessPiece(true, piece);
        currentPiece = piece;
    }

}


void ChessBox::resetOriginalColor() {
    setColor(originalColor);
}

void ChessBox::checkForCheck() {
    int c = 0;
    QList<ChessPiece *> pList = game->alivePiece;
    for (auto &piece : pList) {
        King *king = dynamic_cast<King *>(piece);
        if (king) {
            continue;
        }
        piece->possibleMoves();
        piece->decolor();
        QList<ChessBox *> p1List = piece->getLocation();
        for (auto &piece1 :p1List) {
            King *king1 = dynamic_cast<King *> (piece1->currentPiece);
            if (king1) {
                if (king1->getSide() == piece->getSide()) {
                    continue;
                }
                piece1->setColor(Qt::blue);
                piece->getCurrentBox()->setColor(Qt::darkRed);
                if (!game->check->isVisible()) {
                    game->check->setVisible(true);
                } else {
                    piece1->resetOriginalColor();
                    piece->getCurrentBox()->resetOriginalColor();
//                    game->gameOver();
                    qDebug() << piece->getCurrentBox()->currentPiece;

                }
                c++;
            }
        }

    }

    if (c != 0) {
        game->check->setVisible(false);
        for (auto &piece:pList) {
            piece->getCurrentBox()->resetOriginalColor();
        }
    }
}