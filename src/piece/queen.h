#ifndef QUEEN_H
#define QUEEN_H

#include "chesspiece.h"

class Queen:public ChessPiece
{
public:
    Queen(QString team,QGraphicsItem *parent = nullptr);
    void possibleMoves() override;
};

#endif // QUEEN_H
