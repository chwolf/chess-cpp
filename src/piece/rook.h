#ifndef ROOK_H
#define ROOK_H

#include "chesspiece.h"

class Rook:public ChessPiece
{
public:
    Rook(QString team, QGraphicsItem *parent = nullptr);
    void possibleMoves() override;
    void castling();
    QString direction;
};

#endif // ROOK_H
