#include "rook.h"
#include "src/gamemanager.h"
#include "src/chessboard/chessbox.h"
#include <QDebug>

extern GameManager *game;

Rook::Rook(QString team, QGraphicsItem *parent) : ChessPiece(team, parent) {
    pieceName = "rook";
    setImage();
}

void Rook::possibleMoves() {
    location.clear();
    int x = this->getCurrentBox()->xLoc;
    int y = this->getCurrentBox()->yLoc;

    QString team = this->getSide();

    //up
    for (int i = x, j = y - 1; j >= 0; j--) {
        if (game->collection[i][j]->getChessPieceColor() == team) {
            break;
        } else {
            location.append(game->collection[i][j]);
            if (boxSetting(location.last())) {
                break;
            }
        }
    }
    //right
    for (int i = x + 1, j = y; i <= 7; i++) {
        if (game->collection[i][j]->getChessPieceColor() == team) {
            break;
        } else {
            location.append(game->collection[i][j]);
            if (boxSetting(location.last())) {
                break;
            }
        }

    }
    //down
    for (int i = x, j = y + 1; j <= 7; j++) {
        if (game->collection[i][j]->getChessPieceColor() == team) {
            break;
        } else {
            location.append(game->collection[i][j]);
            if (boxSetting(location.last())) {
                break;
            }
        }
    }
    //left
    for (int i = x - 1, j = y; i >= 0; i--) {
        if (game->collection[i][j]->getChessPieceColor() == team) {
            break;
        } else {
            location.append(game->collection[i][j]);
            if (boxSetting(location.last())) {
                break;
            }
        }

    }
}

void Rook::castling() {
    location.clear();
    int x = this->getCurrentBox()->xLoc;
    int y = this->getCurrentBox()->yLoc;
    if (game->rookToMove->direction == "RIGHT") {
        location.append(game->collection[x - 3][y]);
    } else {
        location.append(game->collection[x + 3][y]);
    }

}

//}
