#ifndef KING_H
#define KING_H

#include "chesspiece.h"
#include "rook.h"

class King:public ChessPiece
{
public:
    King(QString team, Rook *rook, Rook *rook1, QGraphicsItem *parent = nullptr);
    void possibleMoves() override;
    Rook *rook;
    Rook *rook1;
};

#endif // KING_H
