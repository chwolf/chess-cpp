#include "queen.h"

#include "src/gamemanager.h"
#include "src/chessboard/chessbox.h"

extern GameManager *game;
Queen::Queen(QString team,QGraphicsItem *parent):ChessPiece(team,parent)
{
    pieceName = "queen";
    setImage();
}
void Queen::possibleMoves(){
    location.clear();
    int x = this->getCurrentBox()->xLoc;
    int y = this->getCurrentBox()->yLoc;

    QString team = this->getSide();

    //for down right to upper left
    for (int i = x - 1, j = y - 1; i >= 0 && j >= 0; i--, j--) {
        if (game->collection[i][j]->getChessPieceColor() == team) {
            break;
        } else{
            location.append(game->collection[i][j]);
            if (boxSetting(location.last())){
                break;
            }
        }
    }

    //For down left to upper right

    for(int i = x+1,j = y-1; i <= 7 && j >= 0; i++,j--) {
        if(game->collection[i][j]->getChessPieceColor() == team ) {
            break;

        }
        else
        {
            location.append(game->collection[i][j]);
            if(boxSetting(location.last())){
                break;
            }
        }
    }
    //For upper left to down right

    for(int i = x+1,j = y+1; i <= 7 && j <= 7; i++,j++) {
        if(game->collection[i][j]->getChessPieceColor() == team ) {
            break;

        }
        else
        {
            location.append(game->collection[i][j]);
            if(boxSetting(location.last())){
                break;
            }
        }
    }
    //For upper right to down left

    for(int i = x-1,j = y+1; i >= 0 && j <= 7; i--,j++) {
        if(game->collection[i][j]->getChessPieceColor() == team ) {
            break;

        }
        else
        {
            location.append(game->collection[i][j]);
            if(boxSetting(location.last())){
                break;
            }

        }
    }
    //up
    for (int i = x,j=y-1; j>=0;j--) {
        if (game->collection[i][j]->getChessPieceColor() == team){
            break;
        } else{
            location.append(game->collection[i][j]);
            if (boxSetting(location.last())){
                break;
            }
        }
    }
    //right
    for (int i = x+1,j=y; i<=7;i++) {
        if (game->collection[i][j]->getChessPieceColor() == team){
            break;
        } else{
            location.append(game->collection[i][j]);
            if (boxSetting(location.last())){
                break;
            }
        }

    }
    //down
    for (int i = x,j=y+1; j<=7;j++) {
        if (game->collection[i][j]->getChessPieceColor() == team){
            break;
        } else{
            location.append(game->collection[i][j]);
            if (boxSetting(location.last())){
                break;
            }
        }
    }
    //left
    for (int i = x-1,j=y; i>=0;i--) {
        if (game->collection[i][j]->getChessPieceColor() == team){
            break;
        } else{
            location.append(game->collection[i][j]);
            if (boxSetting(location.last())){
                break;
            }
        }

    }
}
