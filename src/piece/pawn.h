#ifndef PAWN_H
#define PAWN_H

#include "chesspiece.h"

class Pawn:public ChessPiece
{
public:
    Pawn(QString team,QGraphicsItem *parent = nullptr);
    void possibleMoves() override;
    void movedTwoSpaces();
    bool moveTwoSpaces = false;
    QString direction;
};

#endif // PAWN_H
