#ifndef KNIGHT_H
#define KNIGHT_H

#include "chesspiece.h"

class Knight:public ChessPiece
{
public:
    Knight(QString team,QGraphicsItem *parent= nullptr);
    void possibleMoves() override;

};

#endif // KNIGHT_H
