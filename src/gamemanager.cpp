#include "gamemanager.h"
#include "src/widgets/button.h"
#include <QPixmap>
#include <QDebug>
#include "src/piece/king.h"

GameManager::GameManager(QWidget *parent) : QGraphicsView(parent) {
    gameScene = new QGraphicsScene();
    gameScene->setSceneRect(0, 0, 1400, 900);


    //Making the view
    setFixedSize(1400, 900);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setScene(gameScene);
//    scale(1,-1);
    QBrush brush;
    brush.setStyle(Qt::SolidPattern);
    brush.setColor(Qt::black);
    setBackgroundBrush(brush);
    pieceToMove = nullptr;

    //display turn
    turnDisplay = new QGraphicsTextItem();
    turnDisplay->setPos(width() / 2 - 100, 10);
    turnDisplay->setZValue(1);
    turnDisplay->setDefaultTextColor(Qt::white);
    turnDisplay->setFont(QFont("arial", 18));
    turnDisplay->setPlainText("Turn : WHITE");

    //display Check
    check = new QGraphicsTextItem();
    check->setPos(width() / 2 - 100, 860);
    check->setZValue(4);
    check->setDefaultTextColor(Qt::red);
    check->setFont(QFont("arial", 18));
    check->setPlainText("CHECK!");
    check->setVisible(false);
    setTurn("WHITE");


}


void GameManager::displayMainMenu() {
    QGraphicsPixmapItem *p = new QGraphicsPixmapItem();
    p->setPixmap(QPixmap(":/images/king1.png"));
    p->setPos(420, 170);
    addToScene(p);
    listG.append(p);

    QGraphicsPixmapItem *p1 = new QGraphicsPixmapItem();
    p1->setPixmap(QPixmap(":/images/king.png"));
    p1->setPos(920, 170);
    addToScene(p1);
    listG.append(p1);
    //Create the title
    QGraphicsTextItem *titleText = new QGraphicsTextItem("Chess Pro");
    QFont titleFont("arial", 50);
    titleText->setFont(titleFont);
    double xPos = width() / 2 - titleText->boundingRect().width() / 2;
    double yPos = 150;
    titleText->setPos(xPos, yPos);
    addToScene(titleText);
    listG.append(titleText);
    //create Button
    Button *playButton = new Button("Play 2 v 2");
    double pxPos = width() / 2 - playButton->boundingRect().width() / 2;
    double pyPos = 300;
    playButton->setPos(pxPos, pyPos);
    connect(playButton, SIGNAL(clicked()), this, SLOT(start()));
    addToScene(playButton);
    listG.append(playButton);

    //Create Quit Button
    Button *quitButton = new Button("Quit");
    double qxPos = width() / 2 - quitButton->boundingRect().width() / 2;
    int qyPos = 375;
    quitButton->setPos(qxPos, qyPos);
    connect(quitButton, SIGNAL(clicked()), this, SLOT(close()));
    addToScene(quitButton);
    drawChessBoard();
    listG.append(quitButton);
}

void GameManager::start() {
    for (int i = 0, n = listG.size(); i < n; i++)
        removeFromScene(listG[i]);

    addToScene(turnDisplay);
    QGraphicsTextItem *whitePiece = new QGraphicsTextItem();
    whitePiece->setPos(70, 10);
    whitePiece->setZValue(1);
    whitePiece->setDefaultTextColor(Qt::white);
    whitePiece->setFont(QFont("arial", 14));
    whitePiece->setPlainText("WHITE PIECE");
    addToScene(whitePiece);

    QGraphicsTextItem *blackPiece = new QGraphicsTextItem();
    blackPiece->setPos(1170, 10);
    blackPiece->setZValue(1);
    blackPiece->setDefaultTextColor(Qt::black);
    blackPiece->setFont(QFont("arial", 14));
    blackPiece->setPlainText("BLACK PIECE");
    addToScene(blackPiece);

    addToScene(check);
    chess->addChessPiece();
}

void GameManager::drawDeadHolder(int x, int y, QColor color) {
    deadHolder = new QGraphicsRectItem(x, y, 300, 900, nullptr);
    QBrush brush;
    brush.setStyle(Qt::SolidPattern);
    brush.setColor(color);
    deadHolder->setBrush(brush);
    addToScene(deadHolder);
}

void GameManager::drawChessBoard() {
    chess = new ChessBoard();
    drawDeadHolder(0, 0, Qt::lightGray);
    drawDeadHolder(1100, 0, Qt::lightGray);
    chess->drawBoxes(width() / 2 - 400, 50);
}

void GameManager::addToScene(QGraphicsItem *item) {
    gameScene->addItem(item);
}

void GameManager::removeFromScene(QGraphicsItem *item) {
    gameScene->removeItem(item);
}

QString GameManager::getTurn() {
    return turn;
}

void GameManager::setTurn(QString value) {
    turn = value;
//    turnDisplay->setPlainText("Trun : "+ getTurn());
}

void GameManager::changeTurn() {
    if (getTurn() == "WHITE") {
        setTurn("BLACK");
    } else {
        setTurn("WHITE");
    }
    turnDisplay->setPlainText("Turn : " + getTurn());
}

void GameManager::displayDeadWhite() {
    int SHIFT = 50;
    int j = 0;
    int k = 0;
    for (auto &whitePiece : whiteDead) {
        if (j == 4) {
            k++;
            j = 0;
        }
        whitePiece->setPos(40 + SHIFT * j++, 100 + SHIFT * 2 * k);
    }
}

void GameManager::displayDeadBlack() {
    int SHIFT = 50;
    int j = 0;
    int k = 0;
    for (auto &blackPiece : blackDead) {
        if (j == 4) {
            k++;
            j = 0;
        }
        blackPiece->setPos(1140 + SHIFT * j++, 100 + SHIFT * 2 * k);
    }
}

void GameManager::placeInDeadPlace(ChessPiece *piece) {
    King *g = dynamic_cast<King *> (piece);
    if (piece->getSide() == "WHITE") {
        whiteDead.append(piece);
        if (g) {
            check->setPlainText("Black Won");
            check->setVisible(true);
            displayDeadWhite();
            gameOver();
        }
        displayDeadWhite();

    } else {
        blackDead.append(piece);
//        King *g = dynamic_cast<King *> (piece);
        if (g) {
            check->setPlainText("White Won");
            check->setVisible(true);
            displayDeadBlack();
            gameOver();
        }
        displayDeadBlack();


    }
    alivePiece.removeAll(piece);
}

void GameManager::gameOver() {
    setTurn("WHITE");
    endGame = true;
    alivePiece.clear();
    blackDead.clear();
    whiteDead.clear();
    chess->reset();
//    removeAll();

//    displayMainMenu();

}

void GameManager::removeAll(){
    QList<QGraphicsItem*> itemsList = gameScene->items();
    for(size_t i = 0, n = itemsList.size();i<n;i++) {
            removeFromScene(itemsList[i]);
    }
}

